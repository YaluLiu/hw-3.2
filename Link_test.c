#include <stdio.h>
#include <stdlib.h>

#include "LinkList.h"

static int success = 1;
static int fail    = 0;

void test_begin(void) 
{
    printf("begin the test----------------- \n");
}

void test_end(void) 
{
    printf("\nend the test------------------- \n");
}

void test_try(int isTrue, const char* msg) 
{
    if(isTrue == success) 
    {
        printf("%s passed the case !Success! \n",msg);
    }
    else
    {
        printf("%s failed to pass the normal case\n",msg);
    }
}

static void test_Link_new_free() 
{
  Link_t s = Link_new();
  test_try(NULL != s, "The func Link_new_free()");
  Link_free(s);
  return ;
}

static void test_Link_put(Link_t head)
{
    printf("\ntest the Link_put--------------\n");
    FILE *fp;
    fp = fopen("Link_put.txt","r");
    if(fp == NULL)  
    {
        printf("file can't open!\n");
        return ;
    }
    int num = 0;char str[20];
    while(fscanf(fp,"%d",&num))
    {
        //printf("scanfing!\n");
        if(num == -1) break;
        fscanf(fp,"%s",str);
//        printf("%d %s",num,str);
        printf("insert the %dth node\n",num);
        int status = Link_put(head,str,num);
        test_try(status != fail,"The func put()");
        // printf("bug1\n");
    }
    fclose(fp);
    Link_print(head);
    return ;
}

static void test_Link_contains(Link_t head)
{
    printf("\ntest the Link_contains---------------\n");
    int status = Link_contains(head,"nunu"); 
    test_try(status != fail,"The func Link_contains()");
        // printf("bug1\n");
    if(status)   
        printf("nunu exist!\n");
    else          
        printf("nunu not exist!\n");
    status = Link_contains(head,"nuli");
    test_try(status != fail,"The func Link_contains()");
        // printf("bug1\n");
    if(status)    
        printf("nuli exist!\n");
    else         
        printf("nuli not exist!\n");
    Link_print(head);
    return ;
}

static void test_Link_get(Link_t head)
{
    printf("\ntest the Link_get---------------\n");
    void* t = Link_get(head,"lili");
    Link_t target = (Link_t)t;
    test_try(target != NULL,"The func get()");
        // printf("bug1\n");
    if(target == NULL)
        printf("lili not exist in the link!\n");
    else
        printf("lili is the %dth\n",target->pvValue);
        // printf("bug1\n");
    target = Link_get(head,"xyz");
    test_try(target != NULL,"The func get()");
    if(target == NULL)
        printf("xyz not exist in the link!\n");
    else
        printf("xyz is the %dth\n",target->pvValue);
    Link_print(head);
    return ;
}

static void test_Link_getLength(Link_t head)
{
    printf("\ntest the Link_getLength---------------\n");
    int Length = Link_getLength(head);
    test_try(Length != fail,"The func getLength()");
    printf("The list's length is %d\n",Length);
    Link_print(head);
    return ;
}

static void test_Link_replace(head)
{
    printf("\ntest the Link_replace---------------\n");
    Link_t target = Link_replace(head,"nunu",10);
    test_try(target != NULL,"The func replace()");
    if(target == NULL)
        printf("nunu not exist in the link!\n");
    else
        printf("nunu is the %dth\n",target->pvValue);
    target = Link_replace(head,"xyz",10);
    test_try(target != NULL,"The func replace()");
    if(target == NULL)
        printf("xyz not exist in the link!\n");
    else
        printf("xyz is the %dth\n",target->pvValue);
    Link_print(head);
    return ;
}

static void test_Link_remove(head)
{
    printf("\ntest the Link_remove---------------\n");
    Link_t thead =head;
    Link_t target = Link_remove(head,"xixi");
//    printf("runing!\n");
    if(target == NULL)
        printf("xixi not exist in the link!\n");
    else
        printf("xixi is the %dth\n",target->pvValue);
    target = Link_remove(head,"xyz");
    if(target == NULL)
        printf("xyz not exist in the link!\n");
    else
        printf("xyz is the %dth\n",target->pvValue);
    Link_print(thead);
    return ;
}

static void test_all()
{
  test_begin();
  test_Link_new_free();
  Link_t head = Link_new();
  test_Link_put(head);
  test_Link_getLength(head);
  test_Link_contains(head);
  test_Link_get(head);
  test_Link_replace(head);
  test_Link_remove(head);
  test_end();
}

int main() {
  test_all();
  return 0;
}


