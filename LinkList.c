#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <malloc.h>

#include "LinkList.h"

//Create a new Link
Link_t Link_new(void)
{
    Link_t head = (Link_t)malloc(sizeof(node));
    if(NULL == head)
    {
        return NULL;
    }
    memset(head,0,sizeof(struct node));
    head->next = NULL;
    return head;
}



//free the memory of the Link
void Link_free(Link_t oLink)
{
    if(oLink == NULL) 
    {
        printf("The List was NULL!\n");
        return ;
    }
    Link_t  temp;
    while(oLink->next)
    {
        temp = oLink->next;
        free(oLink->pcKey);
        memset(oLink,0,sizeof(oLink));
        free(oLink);
        oLink = temp;
    }
    return ;
}

// print the list
void Link_print(Link_t oLink)
{
    printf("the list is :");
    if(oLink == NULL )
    {
        printf("NULL \n");
        return ;
    }
    Link_t temp = oLink;
    printf("head->");
    while(temp->next)
    {
        temp = temp->next;
        printf("(%d,",temp->pvValue);
        printf("%s)->",temp->pcKey);
      //  printf("bug print!\n");
    }
    printf("NULL\n");
}

//insert the pckey.when exist or no memory failed.return 1 if success ,else return 0.
int Link_put(Link_t oLink,
            const char *pcKey,
            const void *pvValue)
{
    if(oLink == NULL) return 0;
    Link_t head = oLink;
    while(head->next)
    {
        head = head->next;
        if(strcmp(pcKey,head->pcKey) == 0) return 0;
    }
    head = oLink;
    Link_t temp = (Link_t)malloc(sizeof(node));
    if(temp == NULL)   return 0;
    memset(temp,0,sizeof(node));
    strcpy(temp->pcKey,pcKey);
    temp->pvValue = pvValue;
//    printf("%s %d",pcKey,pvValue);
    temp->next = head->next;
    head->next = temp;
    return 1;
}

//return the num of the node in the Link
int Link_getLength(Link_t oLink)
{
    if(oLink == NULL) return -1;
    Link_t temp = oLink;
    int cnt = 0;
    while(temp->next)
    {
        ++cnt;
        temp = temp->next;
    }
    return cnt;
}

//find pckey.if found return 1,else return 0.
int Link_contains(Link_t oLink, const char *pcKey)
{
    if(oLink == NULL) return -1;
    Link_t head = oLink;
    while(head->next)
    {
        head = head->next;
        if(strcmp(head->pcKey,pcKey) == 0) return 1;
    }
    return 0;
}

//find pckey.if found return value.else return NULL pointer.
void *Link_get(Link_t oLink, const char *pcKey)
{
    if(oLink == NULL) return NULL;
    Link_t head = oLink;
    while(head->next)
    {
        head = head->next;
        if(strcmp(head->pcKey,pcKey) == 0)
        {
            return head;
        }
    }
    return NULL;
}

//replace the old  value with new value.
void *Link_replace(Link_t oLink,const char *pcKey,const void *pvValue)
{
    Link_t head = oLink;
    while(head->next)
    {
        if(strcmp(head->next->pcKey,pcKey) == 0)
        {
            Link_t phead = head->next;
            Link_t temp = (Link_t)malloc(sizeof(node));
            if(temp == NULL)   return NULL;
            strcpy(temp->pcKey,pcKey);
            temp->pvValue = pvValue;
            temp->next = head->next->next;
            head->next = temp;
            return phead;
        }
        head = head->next;
    }
    return NULL;
}
//remove the old  value 
void *Link_remove(Link_t oLink, const char *pcKey)
{
    Link_t head = oLink;
    while(head->next)
    {
        if(strcmp(head->next->pcKey,pcKey) == 0)
        {
            Link_t phead = head->next;
            head->next = head->next->next;
            return phead;
        }
        head = head->next;
    }
    return NULL;
}


